import unittest
from unittest_extension import UnitTest_Extension

from networkinterface import *

class NetworkInterface_Tests(UnitTest_Extension):
	def test_find_interfaces(self):
		shell_cmd = "ip -o link show | wc -l"
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		amount_found = process.communicate()[0].strip()

		interfaces = find_interfaces()
		self.assertIsInstance(interfaces, list)
		self.assertLength(amount_found, interfaces)
		self.assertContainsOnlyInstances(str, interfaces)

	def test_find_wired_interfaces(self):
		shell_cmd = "ip -o link show | wc -l"
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		total_amount = process.communicate()[0].strip()
		total_amount = int(total_amount)

		shell_cmd = "iw dev | wc -l"
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		wireless_amount = process.communicate()[0].strip()
		wireless_amount = int(wireless_amount)
		wireless_amount /= 5

		amount_found = total_amount - wireless_amount

		interfaces = find_wired_interfaces()
		self.assertIsInstance(interfaces, list)
		self.assertLength(amount_found, interfaces)
		self.assertContainsOnlyInstances(str, interfaces)

	def test_find_wireless_interfaces(self):
		shell_cmd = "iw dev | wc -l"
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		amount_found = process.communicate()[0].strip()
		amount_found = int(amount_found)
		amount_found /= 5

		interfaces = find_wireless_interfaces()
		self.assertIsInstance(interfaces, list)
		self.assertLength(amount_found, interfaces)
		self.assertContainsOnlyInstances(str, interfaces)

	def test_NetworkInterface_init(self):
		pass

	def test_NetworkInterface_get_status(self):
		pass

	def test_NetworkInterface_set_status(self):
		pass

	def test_NetworkInterface_get_name(self):
		pass

	def test_NetworkInterface_set_name(self):
		pass

	def test_NetworkInterface_get_mac_address(self):
		pass

	def test_NetworkInterface_set_mac_address(self):
		pass

	def test_NetworkInterface_get_alias(self):
		pass

	def test_NetworkInterface_set_alias(self):
		pass

	def test_NetworkInterface_get_flags(self):
		pass

	def test_NetworkInterface_set_flag(self):
		pass

	def test_NetworkInterface_exists(self):
		pass
