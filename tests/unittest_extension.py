import os, sys
import unittest

class UnitTest_Extension(unittest.TestCase):
	def assertLength(self, length, collection, msg = None):
		length = int(length)
		collection_length = len(collection)

		self.assertEqual(length, collection_length, msg)

	def assertContainsOnlyInstances(self, cls, collection, msg = None):
		for item in collection:
			self.assertIsInstance(item, cls)

	@classmethod
	def setUpClass(cls):
		if not os.geteuid()==0:
			sys.exit("Permission denied")
