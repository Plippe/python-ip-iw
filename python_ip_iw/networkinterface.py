import re
from subprocess import Popen, PIPE

def find_interfaces():
	shell_cmd = "ip -o link show | grep -oe \"^[0-9]\+: [a-z]*[0-9]*\" | grep -ioe \"[a-z]*[0-9]*$\""
	process = Popen(shell_cmd, shell=True, stdout=PIPE)
	stdout = process.communicate()[0].strip()

	interfaces = []
	for line in stdout.split("\n"):
		interfaces.append(line)

	return interfaces

def find_wired_interfaces():
	all_interfaces = find_interfaces()
	wireless_interfaces = find_wireless_interfaces()

	return list(set(all_interfaces) - set(wireless_interfaces))

def find_wireless_interfaces():
	shell_cmd = "iw dev | grep -i \"interface\" | grep -ioe \"[a-z]*[0-9]*$\""
	process = Popen(shell_cmd, shell=True, stdout=PIPE)
	stdout = process.communicate()[0].strip()

	interfaces = []
	for line in stdout.split("\n"):
		interfaces.append(line)

	return interfaces

class NetworkInterface(object):
	def __init__(self, value):
		if not NetworkInterface.exists(value):
			raise ValueError("\"{}\" is a not valid interface name.".format(value))

		self.name = value

	def get_status(self):
		shell_cmd = "ip link show {} up".format(self.name)
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		stdout = process.communicate()[0].strip()

		return "up" if stdout else "down"

	def set_status(self, value):
		possible_values = ["up", "down"]
		if value not in possible_values:
			raise ValueError("\"{}\" is not a valid status.".format(value))

		return self.__set(value)

	def get_name(self):
		return self.name

	def set_name(self, value):
		if not re.search("^[^ ]*$", value, re.I):
			raise ValueError("\"{}\" is not a valid interface name.".format(value))

		returncode = self.__set("name " + value)
		if returncode:
			self.name = value

		return returncode

	def get_mac_address(self):
		shell_cmd = "ip link show {}".format(self.name)
		shell_cmd = shell_cmd + " | grep -ioe \"[0-9a-f]\{2\}\(:[0-9a-f]\{2\}\)\{5\}\" | grep -m 1 \"\""
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		stdout = process.communicate()[0].strip()

		return stdout

	def set_mac_address(self, value):
		if not re.search("^[0-9a-f]{2}(:[0-9a-f]{2}){5}$", value, re.I):
			raise ValueError("\"{}\" is not a valid address.".format(value))

		return self.__set("address " + value)

	def get_alias(self):
		shell_cmd = "ip link show {} | grep -ioe \"alias [^ ]*\" | grep -ioe \"[^ ]*$\"".format(self.name)
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		stdout = process.communicate()[0].strip()

		return stdout

	def set_alias(self, value):
		if not re.search("^[^ ]*$", value, re.I):
			raise ValueError("\"{}\" is not a valid alias.".format(value))

		return self.__set("alias " + value)

	def get_flags(self):
		shell_cmd = "ip link show dev {} | grep -oe \"<[^>]*>\" | grep -oe \"[^<>]*\"".format(self.name)
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		stdout = process.communicate()[0].strip()

		flags = []
		for flag in stdout.split(","):
			flags.append(flag)

		return flags

	def set_flag(self, name, value):
		possible_names = ["arp", "multicast", "dynamic"]
		if name not in possible_names:
			raise ValueError("\"{}\" is not a valid flag.".format(value))

		possible_values = ["on", "off"]
		if value not in possible_values:
			raise ValueError("\"{}\" is not a valid flag value.".format(value))

		return self.__set("{} {}".format(name, value))

	def __set(self, value):
		shell_cmd = "ip link set dev {} {}".format(self.name, value)
		process = Popen(shell_cmd, shell=True, stdout=PIPE, stderr=PIPE)
		process.wait()

		return True if process.returncode == 0 else False

	@staticmethod
	def exists(value):
		shell_cmd = "ip link show {}".format(value)
		process = Popen(shell_cmd, shell=True, stdout=PIPE, stderr=PIPE)
		process.wait()

		return process.returncode == 0
