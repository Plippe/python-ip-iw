from subprocess import Popen, PIPE
from networkinterface import NetworkInterface

class WirelessNetworkInterface(NetworkInterface):
	def get_mode(self):
		shell_cmd = "iw dev {} info | grep -i \"type\" | grep -ioe \"[a-z]*$\"".format(self.name)
		process = Popen(shell_cmd, shell=True, stdout=PIPE, stderr=PIPE)
		stdout = process.communicate()[0].strip()

		return stdout

	def set_mode(self, value):
		possible_modes = ["monitor", "managed", "station", "wds", "mesh", "mp", "ibss", "adhoc"]

		if value not in possible_modes:
			raise ValueError("\"{}\" is not a valid wireless mode.".format(value))

		shell_cmd = "iw dev {} set type {}".format(self.name, value)
		process = Popen(shell_cmd, shell=True, stdout=PIPE, stderr=PIPE)
		process.wait()

		return True if process.returncode == 0 else False

	def get_power_saver(self):
		shell_cmd = "iw dev {} get power_save | grep -oe \"on$\"".format(self.name)
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		stdout = process.communicate()[0].strip()

		return True if stdout else False

	def set_power_saver(self, value):
		possible_values = ["on", "off"]
		if value not in possible_values:
			raise ValueError("\"{}\" is not a valid power saver value.".format(value))

		return self.__set("power_save " + value)

	def get_channel(self):
		shell_cmd = "iw dev {} info | grep -ioe \"channel [0-9]*\" | grep -oe \"[^ ]$\"".format(self.name)
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		stdout = process.communicate()[0].strip()

		return stdout

	def set_channel(self, value):
		if not 1 <= value <= 14:
			raise ValueError("\"{}\" is not a valid channel.".format(value))

		return self.__set("channel " + str(value))

	def get_frequency(self):
		shell_cmd = "iw dev {} info | grep -ioe \"channel [0-9]* ([^)]*)\" | grep -oe \"(.*)\" | grep -oe \"[0-9]*\"".format(self.name)
		process = Popen(shell_cmd, shell=True, stdout=PIPE)
		stdout = process.communicate()[0].strip()

		return stdout

	def set_frequency(self, value):
		possible_values = [2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447, 2452, 2457, 2462, 2467, 2472, 2484]
		if value not in possible_values:
			raise ValueError("\"{}\" is not a valid frequency value.".format(value))

		return self.__set("freq " + str(value))

	def __set(self, value):
		shell_cmd = "iw dev {} set {}".format(self.name, value)
		process = Popen(shell_cmd, shell=True, stdout=PIPE, stderr=PIPE)
		process.wait()

		return True if process.returncode == 0 else False
