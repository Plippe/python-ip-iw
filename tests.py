import sys

from glob import glob
from unittest import TestSuite, TextTestRunner, defaultTestLoader

sys.path.append("python_ip_iw")
sys.path.append("tests")

def main():
	file_names = glob('tests/*_tests.py')

	suites = []
	for file_name in file_names:
		module_name = file_name[6:-3]
		suites.append(defaultTestLoader.loadTestsFromName(module_name))

	tests = TestSuite(suites)
	TextTestRunner().run(tests)

if __name__ == "__main__":
	main()
