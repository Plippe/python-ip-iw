Python ip and iw mapper
============

A python library to match some of Linux's ip and iw functionalities

Library based on
 - http://linux.die.net/man/8/ip
 - http://linux.die.net/man/8/iw
